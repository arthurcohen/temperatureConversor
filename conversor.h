/* Algoritimo desenvolvido na aula de LP1 (linguagem de programação)
/  Autor: Arthur Cohen
/  Curso: Bacharel em Tenologia da Informacao - UFRN
/  04/03/2017
*/

#ifndef CONVERSOR_H
#define CONVERSOR_H

//definicao das funcoes de conversao de temperaturas
float f2c(float temp);
float c2f(float temp);

#endif
