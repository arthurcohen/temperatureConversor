#  Algoritimo desenvolvido na aula de LP1 (linguagem de programação)
#  Autor: Arthur Cohen
#  Curso: Bacharel em Tenologia da Informacao - UFRN
#  04/03/2017

CC=g++
CFLAGS=-Wall -pedantic
EXEC=exec

all: main clean

main: main.o conversor.o
	$(CC) -o $(EXEC) main.o conversor.o

conversor.o: conversor.cpp conversor.h
	$(CC) -c conversor.cpp conversor.h $(CFLAGS)

main.o: main.cpp
	$(CC) -c main.cpp $(CFLAGS)

clean:
	rm -f *.o *.gch
