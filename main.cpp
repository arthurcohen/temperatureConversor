/* Algoritimo desenvolvido na aula de LP1 (linguagem de programação)
/  Autor: Arthur Cohen
/  Curso: Bacharel em Tenologia da Informacao - UFRN
/  04/03/2017
*/

#include <iostream>
#include "conversor.h"

using namespace std;

int main(void){
    int op = -1; //valor padrao fora do intervalo de opcoes
    float temp;
    do{
        cout<<"CONVERSOR DE TEMPERATURA CELSIUS <--> FAHRENHEIT\n(1) C° -> F°\n(2) F° -> C°\n(-1)CANCELAR\n\nselecione uma opcao:\n";
        cin>>op;
    }while(op<=-2 || op >=3);

    if (op==-1){ //saida forcada da aplicacao
       cout<<"CANCELADO\n";
       return 0;
    }

    cout<<"DIGITE A TEMPERATURA (ex: 30.0 || -3.5 || 95.7):\n";
    cin>>temp;

    if(op==1){ //celsius to fahrenheit
        cout<<c2f(temp)<<endl;
    }else if(op==2){ //fahrenheit to celsius
        cout<<f2c(temp)<<endl;
    }

    return 0;
}
